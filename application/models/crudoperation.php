<?php
	Class crudoperation extends CI_model{
		function __construct(){
			parent::__construct();
		}
		public function datainsertion($table,$data){
			if($this->db->insert($table,$data)){
				return true;
			}else{
				return false;
			}
		}
		public function categoryupdate($id,$data){
			$this->db->where('CategoryId',$id);
			if($this->db->update('category',$data)){
				return true;
			}else{
				return false;
			}
		}
		public function getdata($table){
			$query=$this->db->get($table);
			if($result=$query->result())
				return($result);
			else{
				logg_error('error','Model/crudoperation/getdate');
			}
		}
	}
?>