<?php
	defined('BASEPATH') OR exit('no direct script access allowed');
	class adminpanel extends CI_Controller{
		public function dashboard(){
			if($this->session->userdata('userid')!=NULL){
				//$this->load->model('admin');
				//$data=$this->load->admin();
				$data['title']="Dashboard";
				$this->load->view('template/header',$data);
				$this->load->view('template/leftpanel',$data);
				$this->load->view('admin/dashboard');
				$this->load->view('template/footer');
			}else{
				header('location:'.base_url().'authentication/login');
			}
			
		}
		public function managecategory(){
			if($this->session->userdata('userid')!=NULL){
				$query=$this->db->get('category');
				$data['records']=$query->result();
				$data['title']="Category";
				$this->load->view('template/header',$data);
				$this->load->view('template/leftpanel');
				$this->load->view('admin/managecategory');
				$this->load->view('template/footer');
			}else{
				header('location:'.base_url().'authentication/login');
			}
		}
		public function addcategory(){			
			$file=$_FILES['categoryimage']['name'];
			$categoryName=$this->input->post('categoryname');
			//$imagePrefix=time();			
			//$imageName=$imagePrefix.$value['name'];
			$imageName=time();
			$config['file_name']=$imageName;
			$config['upload_path']='./upload/category/';
			$config['allowed_types']='gif|jpg|jpeg|png';
			$this->load->library('upload',$config);
			if(!$this->upload->do_upload('categoryimage')){
				$error=array('error'=>$this->upload->display_errors());
				//error_log($error);
			}else{
				$categoryImage=$this->upload->data();
				$image="../upload/category/".$categoryImage['file_name'];
				$date=date('Y-m-d H:i:s', time());
				$data=array(
					'CategoryName'=>$categoryName,
					'CategoryImage'=>$image,
					'CategoryAddDate'=>$date,
				);
				echo $image;
				$this->load->model('crudoperation');
				$result=$this->crudoperation->datainsertion('category',$data);
				if($result===TRUE){
					header('location:'.base_url().'Adminpanel/managecategory');
				}else{
					header('location:'.base_url().'Adminpanel/managecategory');
					logg_error('error','addcategory');
				}
			}
		}
		public function editcategory($id){
			if($this->session->userdata('userid')!=NULL){
				$data['title']="Category";
				$query=$this->db->get_where('category',array("CategoryId"=>$id));
				$data['records']=$query->result();
				$this->load->view('template/header',$data);
				$this->load->view('template/leftpanel');
				$this->load->view('admin/editcategory',$data);
				$this->load->view('template/footer');
			}else{
				header('location:'.base_url().'authentication/login');
			}

		}

		public function updatecategory(){
			$categoryName=$this->input->post('categoryname');
			$categoryId=$this->input->post('categoryid');
			$oldImage=$this->input->post('oldimage');	
			//unlink($oldImage);
			$image=$_FILES['image']['name'];
			$imageName=time();
			$config['file_name']=$imageName;
			$config['upload_path']="./upload/category/";
			$config['allowed_types']="gif|jpeg|jpg|png";
			$this->load->library('upload',$config);
			if(!$this->upload->do_upload('image',$config)){
				$error=array('error'=>$this->upload->display_errors());
				show_error($error);
				logg_error('error','admin panelimage upload');
			}else{
				$image=$this->upload->data();
				$categoryImage="../upload/category/".$image['file_name'];
				$updateDate=date('Y-m-d',time());
				$data=array(
					'CategoryName'=>$categoryName,
					'CategoryImage'=>$categoryImage,
					'CategoryUpdateDate'=>$updateDate,
				);
				$this->load->model('crudoperation');
				$result=$this->crudoperation->categoryupdate($categoryId,$data);
				if($result==true){
					header('location:'.base_url().'/adminpanel/managecategory');
				}else{
					echo "error";
					//logg_error('error','editcategory');
				}
			}
		}



		public function manageproject(){
			
			if($this->session->userdata('userid')!=NULL){
				$data['title']="Project";
				$query=$this->db->get('atachement');
				$data['records']=$query->result();
				$this->load->view('template/header',$data);
				$this->load->view('template/leftpanel');
				$this->load->view('admin/manageproject');
				$this->load->view('template/footer');
			}else{
				header('location:'.base_url().'authentication/login');
			}
		}
		public function addproject(){
			if($this->session->userdata('userid')!=NULL){
				$this->load->model('crudoperation');
				$data['records']=$this->crudoperation->getdata('projecttype');
				$data['title']="Add Project";
				$data1['records1']=$this->crudoperation->getdata('category');	
				$this->load->view('template/header',$data);
				$this->load->view('template/leftpanel');
				$this->load->view('admin/addproject',$data1);
				$this->load->view('template/footer');
			}else{
				header('location:'.base_url().'authentication/login');
			}
		}
		public function profile(){
			if($this->session->userdata('userid')!=NULL){
				$data['title']="Profile";
				$userId=$this->session->userdata('userid');
				$query=$this->db->get_where('login',array('UserId'=>$userId));
				$result=$query->result();
				$this->load->view('template/header',$data);
				$this->load->view('template/leftpanel');
				$this->load->view('admin/profile');
				$this->load->view('template/footer');
			}else{
				header('location:'.base_url().'authentication/login');
			}

		}
		public function getcategory(){
			
		}

	}


