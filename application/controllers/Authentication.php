<?php
	defined('BASEPATH') OR exit('no direct script access allowed');
	class authentication extends CI_Controller{
		public function index(){
			$data['title']='Login';
			$userid=$this->session->userdata('userid');
			if($userid==''){
				$this->session->userdata('id');;
				$this->load->view('template/header',$data);
				$this->load->view('signin');
				$this->load->view('template/header');
			}else{
				if($this->session->userdata('role')==='admin'){
					header('location:'.base_url().'adminpanel/dashboard');
				}else{

				}
			}
			
		}
		public function login(){
			$name=$this->input->post('username');
			$password=$this->input->post('password');
			$query=$this->db->get_where("login",array(
				'UserName'=>$name,
				'UserPassword'=>$password
			));
			if($query->num_rows()==0){
				$this->session->set_flashdata('error','Invalid User Name Or Password');
				header('location:'.base_url().$this->index());
			}else{		
				$data=$query->result();
				$this->session->set_userdata('userid',$data[0]->UserId);
				$this->session->set_userdata('role',$data[0]->Role);
				$this->session->set_userdata('name',$data[0]->Name);
				$this->session->set_userdata('email',$data[0]->Email);
				$this->session->set_userdata('mobile',$data[0]->MobileNumber);
				if($data[0]->Role==='admin'){
					echo "Admin";
					header('location:'.base_url().'adminpanel/dashboard'); //Admin Login Start 
				}else{
					echo "User ";  // User Login start here 
				}

			}

		}
		
		public function logout(){
			$this->session->unset_userdata('id');
			$this->session->sess_destroy();
			header('location:'.base_url().'authentication/index');
		}

	}