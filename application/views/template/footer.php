		<script src="<?php echo base_url();?>asset/lib/jquery/jquery.js"></script>
		<script src="<?php echo base_url();?>asset/lib/jquery-ui/jquery-ui.js"></script>
		<script src="<?php echo base_url();?>asset/lib/bootstrap/js/bootstrap.js"></script>
		<script src="<?php echo base_url();?>asset/lib/jquery-toggles/toggles.js"></script>

		<script src="<?php echo base_url();?>asset/lib/morrisjs/morris.js"></script>
		<script src="<?php echo base_url();?>asset/lib/raphael/raphael.js"></script>

		<script src="<?php echo base_url();?>asset/lib/flot/jquery.flot.js"></script>
		<script src="<?php echo base_url();?>asset/lib/flot/jquery.flot.resize.js"></script>
		<script src="<?php echo base_url();?>asset/lib/flot-spline/jquery.flot.spline.js"></script>

		<script src="<?php echo base_url();?>asset/lib/jquery-knob/jquery.knob.js"></script>

		<script src="<?php echo base_url();?>asset/js/quirk.js"></script>
		<script src="<?php echo base_url();?>asset/js/dashboard.js"></script>
	</body>
</html>