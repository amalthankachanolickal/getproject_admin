 <div class="mainpanel">

      <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
          <li><a href="index.html"><i class="fa fa-home mr5"></i>Home</a></li>
          <li class="active">Category</li>
        </ol>


      <div class="row">
          <div class="well well-asset-options clearfix">
            <div class="btn-group pull-right" data-toggle="buttons">
               <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                Add Category
              </button>
            </div>
          </div>
          <div class="row filemanager">
            <?php foreach ($records as $r) {?>
           <div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 image">
              <div class="thmb">                
                <div class="btn-group fm-group">
                  <button type="button" class="btn btn-default dropdown-toggle fm-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu pull-right fm-menu" role="menu">                    
                    <?="<li><a href='".base_url()."/adminpanel/editcategory/".$r->CategoryId."'><i class='fa fa-pencil'></i>Edit</a></li>"?>                    
                    <li><a href=""><i class="fa fa-trash-o"></i>Delete</a></li>
                  </ul>
                </div><!-- btn-group -->
                <div class="thmb-prev ">
                  <a href="">
                   <?="<img src='".$r->CategoryImage."' class='img-responsive' alt='' />"?>
                  </a>
                </div>
                <h4 class="fm-title text-center"><a href=""><?=$r->CategoryName?></a></h4>
                <small class="text-muted">Added: June 30, 2015</small>
              </div><!-- thmb -->
            </div><!-- col-xs-6 -->
            <?php }?>
          </div><!-- row -->  
      </div>
    </div>

  </div><!-- mainpanel -->



<!-- Modal -->
<div class="modal bounceIn animated" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Add Category</h4>
      </div>
      <div class="modal-body">
          <form id="basicForm" action="<?=base_url()?>adminpanel/addcategory" method="POST" class="form-horizontal" enctype="multipart/form-data">
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Category Name <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" name="categoryname" class="form-control" placeholder="Type your Category Name..." required />
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-3 control-label">Category Image <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                      <input type="file" name="categoryimage" class="form-control" placeholder="Type your email..." required />
                    </div>
                  </div>                
                      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-success btn-quirk btn-wide mr5" value="Save" />
       </form>
      </div>
    </div><!-- modal-content -->
  </div><!-- modal-dialog -->
</div><!-- modal -->
<script>
  jQuery(document).ready(function(){

    'use strict';

    jQuery('.thmb').hover(function(){
      var t = jQuery(this);
      t.find('.ckbox').show();
      t.find('.fm-group').show();
    }, function() {
      var t = jQuery(this);
      if(!t.closest('.thmb').hasClass('checked')) {
        t.find('.ckbox').hide();
        t.find('.fm-group').hide();
      }
    });

    jQuery('.ckbox').each(function(){
      var t = jQuery(this);
      var parent = t.parent();
      if(t.find('input').is(':checked')) {
        t.show();
        parent.find('.fm-group').show();
        parent.addClass('checked');
      }
    });


    jQuery('.ckbox').click(function(){
      var t = jQuery(this);
      if(!t.find('input').is(':checked')) {
        t.closest('.thmb').removeClass('checked');
        enable_itemopt(false);
      } else {
        t.closest('.thmb').addClass('checked');
        enable_itemopt(true);
      }
    });

    jQuery('#selectall').click(function(){
      if(jQuery(this).is(':checked')) {
        jQuery('.thmb').each(function(){
          jQuery(this).find('input').attr('checked',true);
          jQuery(this).addClass('checked');
          jQuery(this).find('.ckbox, .fm-group').show();
        });
        enable_itemopt(true);
      } else {
        jQuery('.thmb').each(function(){
          jQuery(this).find('input').attr('checked',false);
          jQuery(this).removeClass('checked');
          jQuery(this).find('.ckbox, .fm-group').hide();
        });
        enable_itemopt(false);
      }
    });

    function enable_itemopt(enable) {
      if(enable) {
        jQuery('.itemopt').removeClass('disabled');
      } else {

        // check all thumbs if no remaining checks
        // before we can disabled the options
        var ch = false;
        jQuery('.thmb').each(function(){
          if(jQuery(this).hasClass('checked'))
            ch = true;
        });

        if(!ch)
          jQuery('.itemopt').addClass('disabled');
      }
    }

    jQuery("a[data-rel^='prettyPhoto']").prettyPhoto();

  });

</script>