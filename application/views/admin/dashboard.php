

  <div class="mainpanel">

    <!--<div class="pageheader">
      <h2><i class="fa fa-home"></i> Dashboard</h2>
    </div>-->
    <div class="contentpanel">
      <div class="row">
        <div class="col-md-9 col-lg-8 dash-left">         
          <div class="row panel-statistics">
            <div class="col-sm-6">
              <div class="panel panel-updates">
                <div class="panel-body">
                  <div class="row">
                    <div class="col-xs-7 col-lg-8">
                      <h4 class="panel-title text-success">Projects Added</h4>
                      <h3>75.7%</h3>
                      <div class="progress">
                        <div style="width: 75.7%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="75.7" role="progressbar" class="progress-bar progress-bar-success">
                          <span class="sr-only">75.7% Complete (success)</span>
                        </div>
                      </div>
                      <p>Added products for this month: 75</p>
                    </div>
                    <div class="col-xs-5 col-lg-4 text-right">
                      <input type="text" value="75" class="dial-success">
                    </div>
                  </div>
                </div>
              </div><!-- panel -->
            </div><!-- col-sm-6 -->

            <div class="col-sm-6">
              <div class="panel panel-success-full panel-updates">
                <div class="panel-body">
                  <div class="row">
                    <div class="col-xs-7 col-lg-8">
                      <h4 class="panel-title text-success">Products Sold</h4>
                      <h3>55.4%</h3>
                      <div class="progress">
                        <div style="width: 55.4%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="55.4" role="progressbar" class="progress-bar progress-bar-info">
                          <span class="sr-only">55.4% Complete (success)</span>
                        </div>
                      </div>
                      <p>Sold products for this month: 1,203</p>
                    </div>
                    <div class="col-xs-5 col-lg-4 text-right">
                      <input type="text" value="55" class="dial-info">
                    </div>
                  </div>
                </div>
              </div><!-- panel -->
            </div><!-- col-sm-6 -->

            <div class="col-sm-6">
              <div class="panel panel-updates">
                <div class="panel-body">
                  <div class="row">
                    <div class="col-xs-7 col-lg-8">
                      <h4 class="panel-title text-danger">Products Returned</h4>
                      <h3>22.1%</h3>
                      <div class="progress">
                        <div style="width: 22.1%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="22.1" role="progressbar" class="progress-bar progress-bar-danger">
                          <span class="sr-only">22.1% Complete (success)</span>
                        </div>
                      </div>
                      <p>Returned products this month: 22</p>
                    </div>
                    <div class="col-xs-5 col-lg-4 text-right">
                      <input type="text" value="22" class="dial-danger">
                    </div>
                  </div>
                </div>
              </div><!-- panel -->
            </div><!-- col-sm-6 -->

          </div><!-- row -->

          
          <div class="row panel-quick-page">
            <div class="col-xs-4 col-sm-5 col-md-4 page-user">            
              <div class="panel">
                <a href="<?=base_url();?>adminpanel/managecategory">
                <div class="panel-heading">
                  <h4 class="panel-title">Add Category</h4>
                </div>
                <div class="panel-body">
                  <div class="page-icon"><i class="icon ion-person-stalker"></i></div>
                </a>               
                </div>
              </div>
            </div>      
            <div class="col-xs-4 col-sm-4 col-md-4 page-products">
              <div class="panel">
                <a href="<?=base_url();?>adminpanel/manageproject">
                <div class="panel-heading">
                  <h4 class="panel-title">Manage Project</h4>
                </div>
                <div class="panel-body">
                  <div class="page-icon"></div>
                </div>
              </a>
              </div>
            </div>
            <div class="col-xs-4 col-sm-3 col-md-2 page-events">
              <div class="panel">
                <div class="panel-heading">
                  <h4 class="panel-title">Events</h4>
                </div>
                <div class="panel-body">
                  <div class="page-icon"><i class="icon ion-ios-calendar-outline"></i></div>
                </div>
              </div>
            </div>
            <div class="col-xs-4 col-sm-3 col-md-2 page-messages">
              <div class="panel">
                <div class="panel-heading">
                  <h4 class="panel-title">Messages</h4>
                </div>
                <div class="panel-body">
                  <div class="page-icon"><i class="icon ion-email"></i></div>
                </div>
              </div>
            </div>
            <div class="col-xs-4 col-sm-5 col-md-2 page-reports">
              <div class="panel">
                <div class="panel-heading">
                  <h4 class="panel-title">Reports</h4>
                </div>
                <div class="panel-body">
                  <div class="page-icon"><i class="icon ion-arrow-graph-up-right"></i></div>
                </div>
              </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-2 page-statistics">
              <div class="panel">
                <div class="panel-heading">
                  <h4 class="panel-title">Statistics</h4>
                </div>
                <div class="panel-body">
                  <div class="page-icon"><i class="icon ion-ios-pulse-strong"></i></div>
                </div>
              </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 page-support">
              <div class="panel">
                <div class="panel-heading">
                  <h4 class="panel-title">Manage Support</h4>
                </div>
                <div class="panel-body">
                  <div class="page-icon"><i class="icon ion-help-buoy"></i></div>
                </div>
              </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-2 page-privacy">
              <div class="panel">
                <div class="panel-heading">
                  <h4 class="panel-title">Privacy</h4>
                </div>
                <div class="panel-body">
                  <div class="page-icon"><i class="icon ion-android-lock"></i></div>
                </div>
              </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-2 page-settings">
              <div class="panel">
                <div class="panel-heading">
                  <h4 class="panel-title">Settings</h4>
                </div>
                <div class="panel-body">
                  <div class="page-icon"><i class="icon ion-gear-a"></i></div>
                </div>
              </div>
            </div>
          </div><!-- row -->

        </div><!-- col-md-9 -->
        </div><!-- col-md-3 -->
      </div><!-- row -->

    </div><!-- contentpanel -->

  </div><!-- mainpanel -->

</section>