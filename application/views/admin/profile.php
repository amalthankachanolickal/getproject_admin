 <div class="mainpanel">

      <div class="contentpanel">

        <div class="row profile-wrapper">
          <div class="col-xs-12 col-md-4 col-lg-2 profile-left">
            <div class="profile-left-heading">
              <ul class="panel-options">
                <li><a><i class="glyphicon glyphicon-option-vertical"></i></a></li>
              </ul>
              <?php 
                foreach ($records as $r) {
                  # code...
                }
              ?>
              <a href="" class="profile-photo"><img class="img-circle img-responsive" src=<?=$r->Image?> alt=""></a>
              <h2 class="profile-name"><?=$this->session->userdata('name')?></h2>
              <ul class="list-group">
                <li class="list-group-item">Posts <a href="timeline.html">1,333</a></li>
                <li class="list-group-item">Following <a href="people-directory.html">541</a></li>
                <li class="list-group-item">Followers <a href="people-directory-grid.html">32,434</a></li>
              </ul>


              <button class="btn btn-danger btn-quirk btn-block profile-btn-follow">Edit Profile</button>
            </div>
            <div class="profile-left-body">
              <h4 class="panel-title">About Me</h4>
              <p><?=$r->AboutUs?></p>

              <hr class="fadeout">

              <h4 class="panel-title">College</h4>
              <p><i class="glyphicon glyphicon-map-marker mr5"></i><?=$r->College?></p>

              <hr class="fadeout">

              <h4 class="panel-title">Programing Skill</h4>
              <p><i class="glyphicon glyphicon-briefcase mr5"></i><?=$r->ProgramingSkill?></p>

              <hr class="fadeout">

              <h4 class="panel-title">Contacts</h4>
              <p><i class="glyphicon glyphicon-phone mr5"></i><?=$r->MobileNumber?></p>

              <hr class="fadeout">

              <h4 class="panel-title">Social</h4>
              <ul class="list-inline profile-social">
                <li><a href=""><i class="fa fa-facebook-official"></i></a></li>
                <li><a href=""><i class="fa fa-twitter"></i></a></li>
                <li><a href=""><i class="fa fa-dribbble"></i></a></li>
                <li><a href=""><i class="fa fa-linkedin"></i></a></li>
              </ul>

            </div>
          </div>
          <div class="col-md-6 col-lg-8 profile-right">
            <div class="profile-right-body">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs nav-justified nav-line">
                <li class="active"><a href="#Qustions" data-toggle="tab"><strong>Qustions</strong></a></li>
                <li><a href="#Projects" data-toggle="tab"><strong>Projects</strong></a></li>
                <li><a href="#Settings" data-toggle="tab"><strong>Settings</strong></a></li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <div class="tab-pane active" id="Qustions">

                  <div class="panel panel-post-item">
                    <div class="panel-heading">
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img alt="" src="<?=base_url()?>asset/images/photos/profilepic.png" class="media-object img-circle">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading">Barbara Balashova</h4>
                          <p class="media-usermeta">
                            <span class="media-time">July 06, 2015 8:30am</span>
                          </p>
                        </div>
                      </div><!-- media -->
                    </div><!-- panel-heading -->
                    <div class="panel-body">
                      <p>As a web designer it’s your job to help users find their way to what they’re looking for. It can be easy to put the needs of your users to one side, but knowing your users, understanding their roles, goals, motives and behavior will confirm how you structure your navigation model. <a href="http://goo.gl/QTccRE" target="_blank">#information</a> <a href="http://goo.gl/QTccRE" target="_blank">#design</a></p>
                      <p>Source: <a href="http://goo.gl/QTccRE" target="_blank">http://goo.gl/QTccRE</a></p>

                    </div>
                    <div class="panel-footer">
                      <ul class="list-inline">
                        <li><a href=""><i class="glyphicon glyphicon-heart"></i> Like</a></li>
                        <li><a><i class="glyphicon glyphicon-comment"></i> Comments (0)</a></li>
                        <li class="pull-right">5 liked this</li>
                      </ul>
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Write some comments">
                    </div>
                  </div><!-- panel panel-post -->
                </div><!-- tab-pane -->

                <div class="tab-pane" id="Projects">
                  Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </div>
                <div class="tab-pane" id="Settings">
                    <div class="panel">
                    <div class="panel-heading">
                      <h4 class="panel-title">Change Password</h4>
                    </div>
                    <div class="panel-body">
                      <div class="form-group has-success">
                        <input type="text" name="oldpassword" id="oldpassword" class="form-control" placeholder="Enter Old Password">
                      </div>
                      <div class="form-group has-warning">
                        <input type="text" name="newpassword" id="newpassword1"class="form-control" placeholder="Enter New Password">
                      </div>
                      <div class="modal-footer">
                        <input type="submit" class="btn btn-success btn-quirk btn-wide mr5" onclick="changepassword()" value="Change Password" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          
        </div><!-- row -->

    </div><!-- contentpanel -->
  </div><!-- mainpanel -->



<script>
$(function() {
  function changepassword(){
    
  }
</script>
