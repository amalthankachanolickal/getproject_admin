<div class="mainpanel">

     <div class="contentpanel">
        <ol class="breadcrumb breadcrumb-quirk">
          <li><a href="index.html"><i class="fa fa-home mr5"></i> Home</a></li>
          <li><a href="buttons.html">Category</a></li>
          <li class="active">Edit Category</li>
        </ol>
        <!-- Modal -->
  <div class="container">
    <div class="modal-content col-md-8">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Edit Category</h4>
      </div>
      <div class="modal-body">
          <?php foreach ($records as $r) {
            # code...
          } ?>
          <form id="basicForm" action="<?=base_url()?>adminpanel/updatecategory" method="POST" class="form-horizontal" enctype="multipart/form-data">
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Category Name <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" name="categoryname" class="form-control" value="<?=$r->CategoryName ?>" required />
                      <input type="hidden" name="categoryid" class="form-control"value="<?=$r->CategoryId?>" required />
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-3 control-label">Category Image <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                      <input type="file" name="image" class="form-control" placeholder="Type your email..." required />
                    </div>
                  </div>                
                      
      </div>
      <div class="modal-footer">
        <a href="<?=base_url()?>/adminpanel/managecategory"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></a>
        <input type="submit" class="btn btn-success btn-quirk btn-wide mr5" value="Save" />
       </form>
      </div>
    </div><!-- modal-content -->
   </div>
  </div><!-- contentpanel -->
 </div><!-- mainpanel -->

				        
      