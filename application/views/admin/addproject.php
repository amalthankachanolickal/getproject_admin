 <div class="mainpanel">

    <div class="contentpanel">

      <ol class="breadcrumb breadcrumb-quirk">
        <li><a href="index.html"><i class="fa fa-home mr5"></i> Home</a></li>
        <li><a href="general-forms.html">Add Project</a></li>
      </ol>

      <div class="row">
        <div class="col-md-2">
          
        </div>
        <div class="col-md-8">
          <div class="panel">
              <div class="panel-heading nopaddingbottom">
                <h4 class="panel-title" align="center">Add Project </h4>
              </div>
              <div class="panel-body">
                <hr>
                <form id="basicForm" action="form-validation.html" class="form-horizontal">
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Project Name <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" name="name" class="form-control" placeholder="Type your project name..." required />
                    </div>
                  </div>
                  <div class="form-group">                
                      <div class="form-group">
                        <label class="col-sm-3 control-label">Project Type <span class="text-danger">* </span></label>
                        <div class="col-sm-8">
                            <select id="select1" class="form-control" data-placeholder="select your project type" style="width: 98%">  
                            <option value="">&nbsp;</option>                      
                              <?php foreach ($records as $r) {
                                  echo "<option value=".$r->TypeName.">".$r->TypeName."</option>";
                              }?>
                          </select>
                        </div>
                      </div>
                  </div>


                  <div class="form-group">
                     <label class="col-sm-3 control-label">Project Description <span class="text-danger">* </span></label>
                    <div class="col-sm-8">
                      <textarea rows="5" class="form-control" name="description" title="Please type a comment at least 6 characters long!" placeholder="Type somthing about your project..." required="" aria-required="true"></textarea>
                    </div>
                  </div>

                    <div class="form-group">                
                      <div class="form-group">
                        <label class="col-sm-3 control-label">Project Catyegory<span class="text-danger">* </span></label>
                        <div class="col-sm-8">
                            <select id="projectcategory" name="projectcategory" class="form-control" data-placeholder="select your project type" style="width: 98%">  
                            <option value="">&nbsp;</option>                      
                              <?php foreach ($records1 as $r1) {
                                  echo "<option value=".$r1->CategoryName.">".$r1->CategoryName."</option>";
                              }?>
                          </select>
                        </div>
                      </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-3 control-label">Attach Project <span class="text-danger">*</span> </label>
                    <div col-md-8>
                      <span class="btn btn-success">
                        <input type="file" name="projectfolder">
                      </span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-3 control-label">URL</label>
                    <div class="col-sm-8">
                      <input type="url" name="url" class="form-control" />
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-3 control-label">Screen Shoot <span class="text-danger">*</span></label>
                    <div class="panel">
            <div class="panel-heading">
              <h4 class="panel-title">Dropzone Multi-File Upload</h4>
              <p>DropzoneJS is an open source library that provides drag'n'drop file uploads with image previews. <a href="http://dropzonejs.com/" target="_blank">http://dropzonejs.com/</a></p>
            </div>
            <div class="panel-body">
              <p>This is just a demo. Uploaded files are <strong>not</strong> stored. This does not handle your file uploads on the server. You have to implement the code to receive and store the file yourself.</p>
              <br />
              <form action="files" class="dropzone">
                <div class="fallback">
                  <input name="file" type="file"/>
                </div>
              </form>
            </div>
          </div>                                                           
                  </div>

                  <hr>

                  <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                      <button class="btn btn-success btn-quirk btn-wide mr5">Submit</button>
                      <button type="reset" class="btn btn-quirk btn-wide btn-default">Reset</button>
                    </div>
                  </div>

                </form>
              </div><!-- panel-body -->
          </div><!-- panel -->

        </div><!-- col-md-6 -->
      </div><!--row -->
    </div><!-- contentpanel -->

  </div><!-- mainpanel -->
<script>
  $(function() {

  // Textarea Auto Resize
  autosize($('#autosize'));

  // Select2 Box
  $('#select1, #select2, #select3').select2();
  $("#select4").select2({ maximumSelectionLength: 2 });
  $("#select5").select2({ minimumResultsForSearch: Infinity });
  $("#select6").select2({ tags: true });

  // Toggles
  $('.toggle').toggles({
    on: true,
    height: 26
  });

  // Input Masks
  $("#date").mask("99/99/9999");
  $("#phone").mask("(999) 999-9999");
  $("#ssn").mask("999-99-9999");

  // Date Picker
  $('#datepicker').datepicker();
  $('#datepicker-inline').datepicker();
  $('#datepicker-multiple').datepicker({ numberOfMonths: 2 });

  // Time Picker
  $('#tpBasic').timepicker();
  $('#tp2').timepicker({'scrollDefault': 'now'});
  $('#tp3').timepicker();

  $('#setTimeButton').on('click', function (){
    $('#tp3').timepicker('setTime', new Date());
  });

  // Colorpicker
  $('#colorpicker1').colorpicker();
  $('#colorpicker2').colorpicker({
    customClass: 'colorpicker-lg',
    sliders: {
      saturation: {
        maxLeft: 200,
        maxTop: 200
      },
      hue: { maxTop: 200 },
      alpha: { maxTop: 200 }
    }
  });

});
</script>

</body>
</html>
