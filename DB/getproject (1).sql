-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 23, 2018 at 01:31 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `getproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `atachement`
--

CREATE TABLE `atachement` (
  `AttachmentId` int(11) NOT NULL,
  `AttachmentTitle` text NOT NULL,
  `AttancmentFile` int(11) NOT NULL,
  `AttachmentDate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `CategoryId` int(11) NOT NULL,
  `CategoryName` varchar(20) NOT NULL,
  `Parent` int(11) NOT NULL,
  `CategoryImage` text NOT NULL,
  `CategoryAddDate` varchar(30) NOT NULL,
  `CategoryUpdateDate` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`CategoryId`, `CategoryName`, `Parent`, `CategoryImage`, `CategoryAddDate`, `CategoryUpdateDate`) VALUES
(21, '.NET', 0, '../upload/category/1531807562.png', '2018-07-13 07:31:51', '2018-07-17'),
(22, 'Php', 0, '../upload/category/1532331152.jpg', '2018-07-17 11:05:28', '2018-07-23'),
(23, 'Angular Js', 0, '../upload/category/1531819929.jpg', '2018-07-17 11:24:00', '2018-07-17'),
(24, 'Android', 0, '../upload/category/1531819888.jpg', '2018-07-17 11:25:08', '2018-07-17'),
(25, 'Angular Js', 0, '../upload/category/1532330128.png', '2018-07-23 09:15:29', '');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `UserId` int(11) NOT NULL,
  `UserName` varchar(20) NOT NULL,
  `UserPassword` varchar(20) NOT NULL,
  `Role` varchar(11) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Email` varchar(40) NOT NULL,
  `College` varchar(30) DEFAULT NULL,
  `University` varchar(30) DEFAULT NULL,
  `ProgramingSkill` varchar(30) DEFAULT NULL,
  `MobileNumber` varchar(15) NOT NULL,
  `Image` varchar(50) NOT NULL,
  `AboutUs` text,
  `RegisterDate` date DEFAULT NULL,
  `UpdateDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`UserId`, `UserName`, `UserPassword`, `Role`, `Name`, `Email`, `College`, `University`, `ProgramingSkill`, `MobileNumber`, `Image`, `AboutUs`, `RegisterDate`, `UpdateDate`) VALUES
(1, 'admin', '123', 'admin', 'Amal Thankachan', 'amalthanakchanolickal@gmail.com', 'Ettumanurappan College', 'M G University', 'C++,PHP', '09539173787', '../upload/category/1531807562.png', 'Use the following code to get all the records from the database. The first statement fetches all the records from “stud” table and returns the object, which will be stored in $query object. The second statement calls the result() function with $query object to get all the records as array.', '2018-07-02', '0000-00-00'),
(2, 'user', '123', 'user', '', '', NULL, NULL, NULL, '0', '0', NULL, NULL, NULL),
(3, 'alex', '123', 'admin', 'Alex Sebastian', 'alexsebastian@gmail.com', 'Ettumanurappan College', 'M G University', 'C++,PHP', '8943434663', '0', 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.\r\n\r\nExcepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\n', '2018-05-08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `projecttype`
--

CREATE TABLE `projecttype` (
  `TypeId` int(11) NOT NULL,
  `TypeName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projecttype`
--

INSERT INTO `projecttype` (`TypeId`, `TypeName`) VALUES
(1, 'Mobile'),
(2, 'Web');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `atachement`
--
ALTER TABLE `atachement`
  ADD PRIMARY KEY (`AttachmentId`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`CategoryId`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`UserId`);

--
-- Indexes for table `projecttype`
--
ALTER TABLE `projecttype`
  ADD PRIMARY KEY (`TypeId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `atachement`
--
ALTER TABLE `atachement`
  MODIFY `AttachmentId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `CategoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `UserId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `projecttype`
--
ALTER TABLE `projecttype`
  MODIFY `TypeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
